export class Matrix {
	private _data: Array<any> = [];

	public get data(): Array<any> {
		return this._data;
	}

	constructor(private gridSize: number) {
		this.init();
	}



	public fill(column: number, row: number, value: any) {
		column--;
		row--;
		this._data[column][row] = value;
	}

	public fillByRange(
		rowcolumn: number,
		start: number,
		stop: number,
		value: any,
		fillColumn = false
	): void {
		start--;
		stop--;
		rowcolumn--;

		for (let i = start; i <= stop; i++) {
			if (!fillColumn) {
				this._data[i][rowcolumn] = value;
			} else {
				this._data[rowcolumn][i] = value;
			}
		}
	}

	public flood(column: number, row: number, value: any): void{
		column--;
		row--;
		const originalArr = [...this._data];
		const originalColour = originalArr[column][row];
		if(originalColour === 0){
			return;
		}
		console.log(`Flood replacing ${originalColour}`);
		// Left cells
		for (let x = column; x >= 0; x--) {
			if(this._data[x][row] != originalColour){
				break;
			}
			if(this._data[x][row] != 0){
				this._data[x][row] = value;

				// verticals
				for (let y = row-1; y >= 0; y--) {
					if(this._data[x][y] != originalColour){
						break;
					}
					this._data[x][y] = value;
				}
				for (let y = row+1; y < this.gridSize; y++) {
					if(this._data[x][y] != originalColour){
						break;
					}
					this._data[x][y] = value;
				}
			}
		}

		// Right cells
		for (let x = column+1; x < this.gridSize; x++) {
			if(this._data[x][row] != originalColour){
				break;
			}
			if(this._data[x][row] != 0){
				this._data[x][row] = value;

				// verticals
				for (let y = row-1; y >= 0; y--) {
					if(this._data[x][y] != originalColour){
						break;
					}
					this._data[x][y] = value;
				}
				for (let y = row+1; y < this.gridSize; y++) {
					if(this._data[x][y] != originalColour){
						break;
					}
					this._data[x][y] = value;
				}
			}
		}

		this._data[column][row] = value;
	}

	private init() {
		this._data = new Array(this.gridSize)
			.fill(0)
			.map(() => new Array(this.gridSize).fill(0));
	}
}
