import { Matrix } from './matrix';

describe('Matrix', () => {
	it('should create an instance', () => {
		expect(new Matrix(1)).toBeTruthy();
	});

	it('should create a 2D array with 20 entries', () => {
		const matrix = new Matrix(20);
		expect(matrix.data[19][0]).toBeDefined();
	});
	it('should fill 4 cells on 2nd row of matrix with `Red` starting from the 3rd column', () => {
		const matrix = new Matrix(20);
		matrix.fillByRange(2, 3, 6, 'Red');
		expect(matrix.data[0][1]).toBe(0);
		expect(matrix.data[1][1]).toBe(0);
		expect(matrix.data[2][1]).toBe('Red');
		expect(matrix.data[3][1]).toBe('Red');
		expect(matrix.data[4][1]).toBe('Red');
		expect(matrix.data[5][1]).toBe('Red');
		expect(matrix.data[6][1]).toBe(0);
	});


	it('should flood all cells to `Blue`', () => {
		const matrix = new Matrix(20);
		// L Shape
		matrix.fillByRange(2, 3, 6, 'Red');
		matrix.fillByRange(2, 2, 6, 'Red', true);
		matrix.flood(3,2, 'Blue');

		expect(matrix.data[0][1]).toBe(0);
		expect(matrix.data[1][1]).toBe('Blue');
		expect(matrix.data[2][1]).toBe('Blue');
		expect(matrix.data[3][1]).toBe('Blue');
		expect(matrix.data[4][1]).toBe('Blue');
		expect(matrix.data[5][1]).toBe('Blue');
		expect(matrix.data[6][1]).toBe(0);
	});
});
