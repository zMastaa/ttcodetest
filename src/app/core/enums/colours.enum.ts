export enum ColoursEnum {

	// These could/should be `Number` type but I thought it would be easier for readibility for the reviewer in the console log
	blue = 'Blue',
	yellow = 'Yellow',
	red = 'Red',
	green = 'Green'
}
