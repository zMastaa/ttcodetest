import { Component } from '@angular/core';
import { ColoursEnum } from './core/enums/colours.enum';
import { Matrix } from './shared/matrix';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent {
	public title = 'Code Test';

	constructor() {
		const colours = ColoursEnum;

		// Simple
		const simple = new Matrix(9);
		simple.fill(8, 2, colours.yellow);
		simple.fill(8, 4, colours.blue);
		simple.fill(9, 5, colours.blue);
		console.log(simple.data);

		// Simple Fill by Range

		const simpleFlood = new Matrix(9);
		simpleFlood.fill(8, 2, colours.yellow);
		simpleFlood.fill(8, 4, colours.blue);
		simpleFlood.fill(9, 5, colours.blue);
		simpleFlood.fillByRange(3, 2, 7, colours.blue);
		simpleFlood.fillByRange(4, 5, 6, colours.blue);
		console.log(simpleFlood.data);

		// Simple Range Fill Vertical Green

		const rangeVerticals = new Matrix(9);
		rangeVerticals.fill(8, 2, colours.yellow);
		rangeVerticals.fill(8, 4, colours.blue);
		rangeVerticals.fill(9, 5, colours.blue);
		rangeVerticals.fillByRange(3, 2, 7, colours.blue);
		rangeVerticals.fillByRange(4, 5, 6, colours.blue);
		rangeVerticals.fillByRange(3, 2, 6, colours.green, true);
		console.log(rangeVerticals.data);

		// Flood Red to replace blue
		const flood = new Matrix(9)

		flood.fill(8, 2, colours.yellow);
		flood.fill(8, 4, colours.blue);
		flood.fill(9, 5, colours.blue);
		flood.fillByRange(3, 2, 7, colours.blue);
		flood.fillByRange(4, 5, 6, colours.blue);
		flood.fillByRange(3, 2, 6, colours.green, true);
		flood.flood(6,3, colours.red)
		console.log(flood.data);
	}
}
